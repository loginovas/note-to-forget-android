package iloginovas.notetoforget.ui

import android.app.Application
import iloginovas.notetoforget.ui.common.LoggedInScopeViewModel
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import org.koin.core.scope.Scope

sealed class Action {
    object OpenNoteEditorForNewNote : Action()
    class OpenNoteEditorForNote(val noteId: Long) : Action()
}

class MainFragmentViewModel(
    app: Application,
    scope: Scope
) : LoggedInScopeViewModel(app, scope) {

    private val _actionFlow = MutableSharedFlow<Action>(
        extraBufferCapacity = 16,
        onBufferOverflow = BufferOverflow.DROP_LATEST
    )
    val actionFlow: SharedFlow<Action> get() = _actionFlow

    fun triggerAction(action: Action) {
        _actionFlow.tryEmit(action)
    }
}