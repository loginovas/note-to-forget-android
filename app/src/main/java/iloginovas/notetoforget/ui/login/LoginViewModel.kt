package iloginovas.notetoforget.ui.login

import android.app.Application
import androidx.lifecycle.*
import iloginovas.notetoforget.R
import iloginovas.notetoforget.common.login.*
import iloginovas.notetoforget.common.validation.PasswordErrorCause
import iloginovas.notetoforget.common.validation.PasswordValidator
import iloginovas.notetoforget.common.validation.UsernameErrorCause
import iloginovas.notetoforget.common.validation.UsernameValidator
import iloginovas.notetoforget.data.LoginResult
import iloginovas.notetoforget.data.model.UserCredentials
import iloginovas.notetoforget.data.repository.LoginRepository
import iloginovas.notetoforget.data.Preferences
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*

enum class Mode { LOGIN, REGISTRATION }

sealed class ErrorMessage {
    object None : ErrorMessage()
    class Existing(val localizedErrorMsg: String) : ErrorMessage()
}

fun ErrorMessage.errorMsgOrEmpty(): String =
    if (this is ErrorMessage.Existing) this.localizedErrorMsg else ""


interface ILoginViewModel {
    fun setUsername(username: String)
    val usernameErrorFlow: Flow<ErrorMessage>

    fun getRecentUsername(): String?

    fun setPassword(password: String)
    val passwordErrorFlow: Flow<ErrorMessage>

    fun switchMode()
    val modeFlow: StateFlow<Mode>

    fun startLogin()
    fun cancelLogging()
    val loggingInProgressFlow: Flow<Boolean>
    val errorFlow: StateFlow<ErrorMessage>

    val loggedIn: StateFlow<LoginResult.Successful?>
}


// Implementation
class LoginViewModel(
    application: Application,
    private val loginRepository: LoginRepository,
    private val preferences: Preferences
) : AndroidViewModel(application), ILoginViewModel {

    private val usernameFlow = MutableStateFlow("")
    override val usernameErrorFlow: Flow<ErrorMessage> =
        usernameFlow.map { validateUsername(it) }

    private val passwordFlow = MutableStateFlow("")
    override val passwordErrorFlow: Flow<ErrorMessage> =
        passwordFlow.map { validatePassword(it) }

    private val _modeFlow = MutableStateFlow<Mode>(Mode.LOGIN)
    override val modeFlow: StateFlow<Mode>
        get() = _modeFlow

    // Should be accessed only from UI thread
    private val loginJobFlow: MutableStateFlow<Job?> = MutableStateFlow(null)
    override val loggingInProgressFlow: Flow<Boolean> =
        loginJobFlow.map { it != null }

    private val _errorFlow: MutableStateFlow<ErrorMessage> = MutableStateFlow(ErrorMessage.None)
    override val errorFlow: StateFlow<ErrorMessage>
        get() = _errorFlow

    private val _loggedIn: MutableStateFlow<LoginResult.Successful?> = MutableStateFlow(null)
    override val loggedIn: StateFlow<LoginResult.Successful?>
        get() = _loggedIn


    override fun switchMode() {
        val newMode = when (_modeFlow.value) {
            Mode.LOGIN -> Mode.REGISTRATION
            Mode.REGISTRATION -> Mode.LOGIN
        }
        _modeFlow.value = newMode
    }

    override fun setUsername(username: String) {
        usernameFlow.value = username
    }

    override fun setPassword(password: String) {
        passwordFlow.value = password
    }

    override fun getRecentUsername(): String? =
        preferences.loginScreenRecentUsername.value

    private fun setRecentUsername(username: String) {
        preferences.loginScreenRecentUsername.value = username
    }

    override fun startLogin() {
        if (loginJobFlow.value != null) return

        val job: Job = createLoginJob()
        job.invokeOnCompletion { loginJobFlow.value = null }
        _errorFlow.value = ErrorMessage.None
        loginJobFlow.value = job
        job.start()
    }

    override fun cancelLogging() {
        loginJobFlow.value?.cancel()
    }

    private fun createLoginJob(): Job =
        viewModelScope.launch(start = CoroutineStart.LAZY) {
            try {
                val username = usernameFlow.value.trim()
                val password = passwordFlow.value.trim()
                val userCredentials = UserCredentials(username, password)

                val loginResult: LoginResult = when (modeFlow.value) {
                    Mode.LOGIN -> loginRepository.login(userCredentials)
                    Mode.REGISTRATION -> loginRepository.register(userCredentials)
                }
                when (loginResult) {
                    is LoginResult.Successful -> loginHandler.onSuccess(loginResult)
                    is LoginResult.Failed -> loginHandler.onFailure(loginResult)
                }
            } catch (e: CancellationException) {
                loginHandler.onCancellation()
            } catch (e: Throwable) {
                loginHandler.onException(e)
            }
        }

    private val loginHandler = object {

        fun onSuccess(result: LoginResult.Successful) {
            setRecentUsername(result.user.name)
            _loggedIn.value = result
        }

        fun onCancellation() {
            val errorMsg = context.getString(R.string.login_loggingWasCanceled)
            _errorFlow.value = ErrorMessage.Existing(errorMsg)
        }

        fun onException(e: Throwable) {
            val errorMsg = context.getString(R.string.login_unknownLoggingError)
            _errorFlow.value = ErrorMessage.Existing(errorMsg)
        }

        fun onFailure(result: LoginResult.Failed) {
            when (val errorCause: LoginErrorCause = result.errorCause) {
                is WrongCredentials -> {
                    val errorMsg = errorCause.localizedMsg(context)
                    _errorFlow.value = ErrorMessage.Existing(errorMsg)
                }
                is UsernameAlreadyExists -> {
                    val errorMsg = errorCause.localizedMsg(context)
                    _errorFlow.value = ErrorMessage.Existing(errorMsg)
                }
                is InvalidFormat -> {
                    val usernameError: String? =
                        errorCause.usernameError.let {
                            if (it == UsernameErrorCause.USERNAME_IS_EMPTY) it.localizedMsg(context)
                            else null
                        }
                    val passwordError: String? =
                        errorCause.passwordError.let {
                            if (it == PasswordErrorCause.PASSWORD_IS_EMPTY) it.localizedMsg(context)
                            else null
                        }
                    val errorMsg = sequenceOf(usernameError, passwordError)
                        .filterNotNull()
                        .joinToString(separator = "\n")

                    _errorFlow.value =
                        if (errorMsg.isBlank()) ErrorMessage.None
                        else ErrorMessage.Existing(errorMsg)
                }
            }
        }
    }
}


private fun AndroidViewModel.validateUsername(username: String): ErrorMessage =
    when (val errorCause: UsernameErrorCause? = UsernameValidator.validate(username)) {
        null,
        UsernameErrorCause.USERNAME_IS_EMPTY ->
            ErrorMessage.None
        else ->
            ErrorMessage.Existing(errorCause.localizedMsg(context))
    }

private fun AndroidViewModel.validatePassword(password: String): ErrorMessage =
    when (val errorCause: PasswordErrorCause? = PasswordValidator.validate(password)) {
        null,
        PasswordErrorCause.PASSWORD_IS_EMPTY ->
            ErrorMessage.None
        else ->
            ErrorMessage.Existing(errorCause.localizedMsg(context))
    }

private val AndroidViewModel.context: Application
    inline get() = getApplication()
