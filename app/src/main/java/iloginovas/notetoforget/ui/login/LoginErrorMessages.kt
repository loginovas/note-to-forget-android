package iloginovas.notetoforget.ui.login

import android.content.Context
import iloginovas.notetoforget.R
import iloginovas.notetoforget.common.login.UsernameAlreadyExists
import iloginovas.notetoforget.common.login.WrongCredentials
import iloginovas.notetoforget.common.validation.PasswordErrorCause
import iloginovas.notetoforget.common.validation.UsernameErrorCause

fun WrongCredentials.localizedMsg(context: Context): String =
    context.getString(R.string.login_errorMsg_invalidCredentials)

fun UsernameAlreadyExists.localizedMsg(context: Context): String =
    context.getString(R.string.login_errorMsg_registrationError_usernameAlreadyExists)

fun UsernameErrorCause.localizedMsg(context: Context): String =
    with(context) {
        when (this@localizedMsg) {
            UsernameErrorCause.USERNAME_IS_EMPTY ->
                getString(R.string.login_errorMsg_usernameIsEmpty)
            UsernameErrorCause.USERNAME_CONTAINS_INVALID_CHARACTERS ->
                getString(R.string.login_errorMsg_usernameContainsInvalidCharacters)
            UsernameErrorCause.MAX_LENGTH_EXCEEDED ->
                getString(R.string.login_errorMsg_maxLengthExceeded)
            UsernameErrorCause.FIRST_CHARACTER_IS_NUMBER ->
                getString(R.string.login_errorMsg_firstCharacterShouldNotBeNumber)
            UsernameErrorCause.USERNAME_SHOULD_CONTAIN_AT_LEAST_ONE_LETTER ->
                getString(R.string.login_errorMsg_atLeastOneLetterIsExpected)
        }
    }

fun PasswordErrorCause.localizedMsg(context: Context): String =
    with(context) {
        when (this@localizedMsg) {
            PasswordErrorCause.PASSWORD_IS_EMPTY ->
                getString(R.string.login_errorMsg_passwordIsEmpty)
            PasswordErrorCause.PASSWORD_CONTAINS_INVALID_CHARACTERS ->
                getString(R.string.login_errorMsg_passwordDoesNotMatchToFormat)
            PasswordErrorCause.PASSWORD_IS_TOO_SHORT ->
                getString(R.string.login_errorMsg_passwordIsShort)
            PasswordErrorCause.PASSWORD_IS_TOO_LONG ->
                getString(R.string.login_errorMsg_passwordIsTooLong)
        }
    }