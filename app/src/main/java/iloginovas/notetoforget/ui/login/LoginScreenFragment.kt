package iloginovas.notetoforget.ui.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import iloginovas.notetoforget.R
import iloginovas.notetoforget.databinding.LoginFormBinding
import iloginovas.notetoforget.databinding.LoginScreenBinding
import iloginovas.notetoforget.data.LoginResult
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class LoginScreenFragment : Fragment() {

    interface Callback {
        fun loggedIn(loginResult: LoginResult.Successful)
    }

    companion object {
        fun newInstance(): LoginScreenFragment {
            return LoginScreenFragment()
        }
    }

    private val viewModel: LoginViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = LoginScreenBinding.inflate(inflater, container, false)
        initView(binding, savedInstanceState)
        ContentViewSizeOptimizationSupport.createAndStart(binding)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (requireActivity() as AppCompatActivity).supportActionBar?.hide()
    }

    private fun initView(binding: LoginScreenBinding, savedInstanceState: Bundle?) {
        val loginForm: LoginFormBinding = binding.loginFormPanel

        loginForm.usernameTextInput.doAfterTextChanged {
            viewModel.setUsername(it.toString())
        }

        if (savedInstanceState == null) {
            val recentUsername = viewModel.getRecentUsername() ?: ""
            viewModel.setUsername(recentUsername)
        }

        loginForm.passwordTextInput.doAfterTextChanged {
            viewModel.setPassword(it.toString())
        }

        loginForm.switchModeButton.setOnClickListener { viewModel.switchMode() }
        loginForm.loginButton.setOnClickListener { viewModel.startLogin() }
        binding.cancelButton.setOnClickListener { viewModel.cancelLogging() }

        viewModel.usernameErrorFlow.launchCollect {
            loginForm.usernameTextInputLayout.error = it.errorMsgOrEmpty()
        }

        viewModel.passwordErrorFlow.launchCollect {
            loginForm.passwordTextInputLayout.error = it.errorMsgOrEmpty()
        }

        viewModel.modeFlow.launchCollect {
            onModeChanged(it, binding.loginFormPanel)
        }

        viewModel.loggingInProgressFlow.launchCollect {
            onLoggingProgressStateChanged(it, binding)
        }

        viewModel.loggedIn.filterNotNull().launchCollect {
            (context as? Callback)?.loggedIn(it)
        }

        viewModel.errorFlow.combine(viewModel.loggingInProgressFlow, ::Pair)
            .launchCollect {
                val (error: ErrorMessage, loggingInProgress: Boolean) = it
                if (loggingInProgress)
                    binding.loginErrorPanel.visibility = View.GONE
                else when (error) {
                    is ErrorMessage.None ->
                        binding.loginErrorPanel.visibility = View.GONE
                    is ErrorMessage.Existing -> {
                        binding.loginErrorMsgTextView.text = error.localizedErrorMsg
                        binding.loginErrorPanel.visibility = View.VISIBLE
                    }
                }
            }
    }

    private inline fun <T> Flow<T>.launchCollect(
        crossinline collector: suspend (value: T) -> Unit
    ) {
        val flow = this
        viewLifecycleOwner.lifecycleScope.launch { flow.collect(collector) }
    }

    private fun onModeChanged(mode: Mode, loginForm: LoginFormBinding) {
        val loginButton: Button = loginForm.loginButton
        val switchModeButton: Button = loginForm.switchModeButton
        when (mode) {
            Mode.REGISTRATION -> {
                loginButton.text = getString(R.string.login_registerButton_label)
                switchModeButton.text = getString(R.string.login_switchButton_goToLogin)
            }
            Mode.LOGIN -> {
                loginButton.text = getString(R.string.login_loginButton_label)
                switchModeButton.text = getString(R.string.login_switchButton_goToRegistration)
            }
        }
    }

    private fun onLoggingProgressStateChanged(
        isLoggingInProgress: Boolean,
        binding: LoginScreenBinding
    ) {
        binding.loginFormContainer.visibility =
            if (isLoggingInProgress) View.GONE else View.VISIBLE

        binding.loginProgressGroup.visibility =
            if (isLoggingInProgress) View.VISIBLE else View.GONE
    }
}

/**
 * Supports hiding the login header if there is not enough screen space
 * (especially when virtual keyboard is shown)
 * */
private class ContentViewSizeOptimizationSupport private constructor(
    val binding: LoginScreenBinding
) {
    private val minHeight: Int = calcMinHeight()

    companion object {
        fun createAndStart(binding: LoginScreenBinding) =
            ContentViewSizeOptimizationSupport(binding).apply {
                start()
            }
    }

    fun start() {
        binding.root.addOnLayoutChangeListener { view,
                                                 left, top, right, bottom,
                                                 oldLeft, oldTop, oldRight, oldBottom ->
            val newHeight = bottom - top
            onWindowHeightChange(newHeight)
        }
    }

    private fun onWindowHeightChange(newHeight: Int) {
        Timber.d("onWindowHeightChange: newHeight=$newHeight")
        val isHeaderVisible = newHeight >= minHeight
        binding.loginHeader.apply {
            if (isVisible != isHeaderVisible) {
                isVisible = isHeaderVisible
                post {
                    requestLayout()
                }
            }
        }
    }

    private fun calcMinHeight(): Int {
        val componentsToBeMeasured = with(binding) {
            listOf(
                loginHeader, loginFormPanel.root, loginErrorPanel
            )
        }

        var measuredHeight = 0
        componentsToBeMeasured.forEach {
            it.measure(
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
            )
            measuredHeight += it.measuredHeight

            (it.layoutParams as? ViewGroup.MarginLayoutParams)?.apply {
                measuredHeight += bottomMargin + topMargin
            }
        }

        Timber.d("calcMinHeight: measuredHeight=$measuredHeight")
        return measuredHeight
    }
}