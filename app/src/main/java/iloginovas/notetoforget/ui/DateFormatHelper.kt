package iloginovas.notetoforget.ui

import android.content.Context
import android.text.format.DateFormat
import java.util.*

/**
 * **Concurrency:** Date formats are not synchronized.
 * It is recommended to create separate format instances for each thread.
 * If multiple threads access a format concurrently, it must be synchronized
 * externally.
 */
class DateFormatHelper(context: Context) {

    val dateFormatMedium: java.text.DateFormat = DateFormat.getMediumDateFormat(context)
    val dateFormatLong: java.text.DateFormat = DateFormat.getLongDateFormat(context)
    val timeFormat: java.text.DateFormat = DateFormat.getTimeFormat(context)

    fun formatDateMedium(date: Date): String = dateFormatMedium.format(date)
    fun formatDateLong(date: Date): String = dateFormatMedium.format(date)
    fun formatTime(date: Date): String = timeFormat.format(date)

    fun formatDateTime(date: Date): String {
        val datePart = dateFormatMedium.format(date)
        val timePart = timeFormat.format(date)
        return "$datePart $timePart"
    }
}