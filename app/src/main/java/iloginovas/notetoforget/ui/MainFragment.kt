package iloginovas.notetoforget.ui

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.commit
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.lifecycleScope
import iloginovas.notetoforget.R
import iloginovas.notetoforget.data.Preferences
import iloginovas.notetoforget.data.db.AppDatabase
import iloginovas.notetoforget.ui.common.LoggedInScopeFragment
import iloginovas.notetoforget.ui.notes.NotesFragment
import iloginovas.notetoforget.ui.notes.editor.NoteEditorFragment
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class MainFragment : LoggedInScopeFragment(R.layout.fragment_main) {

    interface Callback {
        fun loggedOut()
    }

    companion object {
        fun newInstance() = MainFragment()
    }

    private val viewModel: MainFragmentViewModel by viewModel()
    private val prefs: Preferences by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.main_screen, menu)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Make app bar visible
        (requireActivity() as AppCompatActivity).supportActionBar?.apply {
            setTitle(R.string.notes_appBarTitle)
            show()
        }

        if (savedInstanceState == null) {
            childFragmentManager.commit {
                val childFragment = NotesFragment.newInstance()
                add(R.id.loggedInScreen_fragmentContainer, childFragment)
            }
        }

        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.actionFlow.collect { action: Action ->
                when (action) {
                    is Action.OpenNoteEditorForNewNote -> {
                        childFragmentManager.commit {
                            val childFragment = NoteEditorFragment.createForNewNote()
                            setReorderingAllowed(true)
                            setCustomAnimations(
                                R.anim.slide_in, R.anim.slide_out,
                                R.anim.slide_in_reverse, R.anim.slide_out_reverse,
                            )
                            replace(R.id.loggedInScreen_fragmentContainer, childFragment)
                            addToBackStack(null)
                        }
                    }
                    is Action.OpenNoteEditorForNote -> {
                        childFragmentManager.commit {
                            val childFragment = NoteEditorFragment.createForNote(action.noteId)
                            setReorderingAllowed(true)
                            setCustomAnimations(
                                R.anim.slide_in, R.anim.slide_out,
                                R.anim.slide_in_reverse, R.anim.slide_out_reverse,
                            )
                            replace(R.id.loggedInScreen_fragmentContainer, childFragment)
                            addToBackStack(null)
                        }
                    }
                }
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
        when (item.itemId) {
            R.id.logout_menu_item -> {
                val userId = prefs.userSession.value?.userId
                if (userId != null)
                    lifecycle.addObserver(DatabaseCleaner(userId))

                (activity as? Callback)?.loggedOut()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }

    inner class DatabaseCleaner(private val userId: Int) : LifecycleObserver {

        @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
        fun clean() {
            try {
                Timber.d("Start AppDatabase.deleteDatabase")
                AppDatabase.deleteDatabase(requireContext().applicationContext, userId)
                scope.close()
                Timber.d("End AppDatabase.deleteDatabase")
            } catch (e: Throwable) {
                Timber.e(e)
            }
        }
    }
}