package iloginovas.notetoforget.ui.common

import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import iloginovas.notetoforget.App
import iloginovas.notetoforget.data.Preferences
import iloginovas.notetoforget.data.UserSession
import iloginovas.notetoforget.data.getOrCreateUserSessionScope
import org.koin.core.component.KoinScopeComponent
import org.koin.core.scope.Scope

abstract class LoggedInScopeFragment : Fragment, KoinScopeComponent {

    constructor() : super()
    constructor(@LayoutRes contentLayoutId: Int) : super(contentLayoutId)

    override val scope: Scope by lazy {
        val preferences: Preferences = App.koin.get()
        val userSession: UserSession = preferences.userSession.value!!
        getKoin().getOrCreateUserSessionScope(userSession)
    }
}