package iloginovas.notetoforget.ui.common

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import iloginovas.notetoforget.data.repository.NoteRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import org.koin.core.scope.Scope

abstract class LoggedInScopeViewModel(
    app: Application,

    /**
     * Logged-in (UserSession) scope [iloginovas.notetoforget.DI.LOGGED_IN_SCREEN_DEPENDENCIES]
     */
    private val scope: Scope

) : AndroidViewModel(app) {

    private val _repository = MutableStateFlow<NoteRepository?>(null)

    init {
        initRepositoryInBackground()
    }

    suspend fun getRepository(): NoteRepository {
        return _repository.value ?: _repository.filterNotNull().first()
    }

    private fun initRepositoryInBackground() {
        viewModelScope.launch(Dispatchers.IO) {
            val noteRepository: NoteRepository = scope.get()
            _repository.value = noteRepository
        }
    }
}