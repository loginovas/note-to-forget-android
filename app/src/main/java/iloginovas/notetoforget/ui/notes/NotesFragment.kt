package iloginovas.notetoforget.ui.notes

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.DividerItemDecoration.VERTICAL
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import iloginovas.notetoforget.R
import iloginovas.notetoforget.data.model.SyncStatus
import iloginovas.notetoforget.databinding.NotesFragmentBinding
import iloginovas.notetoforget.ui.Action
import iloginovas.notetoforget.ui.DateFormatHelper
import iloginovas.notetoforget.ui.MainFragmentViewModel
import iloginovas.notetoforget.ui.common.LoggedInScopeFragment
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import org.koin.android.ext.android.get
import org.koin.androidx.viewmodel.ext.android.getViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class NotesFragment : LoggedInScopeFragment(R.layout.notes_fragment) {

    companion object {
        fun newInstance() = NotesFragment()
    }

    private val mainViewModel: MainFragmentViewModel by lazy {
        requireParentFragment().getViewModel()
    }
    private val viewModel: NotesViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.notes_fragment, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
        when (item.itemId) {
            R.id.new_note_menu_item -> {
                mainViewModel.triggerAction(Action.OpenNoteEditorForNewNote)
                true
            }
            R.id.sync_menu_item -> {
                viewModel.launchSync()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (requireActivity() as AppCompatActivity).supportActionBar?.apply {
            setTitle(R.string.notes_appBarTitle)
            this.setDisplayHomeAsUpEnabled(false)
        }

        val noteAdapter = NoteAdapter(get(), noteItemClickListener = { note ->
            mainViewModel.triggerAction(Action.OpenNoteEditorForNote(note.id))
        })

        val recyclerView: RecyclerView = view.findViewById(R.id.notes_recyclerView)
        recyclerView.addItemDecoration(DividerItemDecoration(requireContext(), VERTICAL))
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        recyclerView.adapter = noteAdapter

        viewLifecycleOwner.lifecycleScope.launch {
            val viewModelReady: NotesViewModel.ReadyState = viewModel.state
                .transform { state: NotesViewModel.State ->
                    when (state) {
                        is NotesViewModel.ReadyState -> emit(state)
                        else -> return@transform
                    }
                }
                .first()

            onViewModelInitialized(viewModelReady, noteAdapter)
        }
    }

    private fun onViewModelInitialized(
        viewModelReady: NotesViewModel.ReadyState,
        noteAdapter: NoteAdapter
    ) {
        viewLifecycleOwner.lifecycleScope.launch {
            viewModelReady.notesFlow.collectLatest { pagingData ->
                noteAdapter.submitData(pagingData)
            }
        }

        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            val bind = NotesFragmentBinding.bind(requireView())
            val dateFormatHelper: DateFormatHelper = scope.get()

            viewModelReady.syncStatusFlow.collect { syncStatus: SyncStatus ->
                when (syncStatus) {
                    is SyncStatus.NeverSynced -> {
                        bind.notesStatusPanelSyncProgressBar.visibility = View.GONE
                        bind.notesStatusPanelLastUpdateInfo.visibility = View.VISIBLE
                        bind.notesStatusPanelUnsavedChangesInfo.visibility = View.GONE

                        bind.notesStatusPanelLastUpdateInfo.text =
                            getString(R.string.notes_statusPanel_neverSynced)

                        // Start sync if never synced before
                        viewModel.launchSync()
                    }
                    is SyncStatus.InProgress -> {
                        bind.notesStatusPanelSyncProgressBar.visibility = View.VISIBLE
                        bind.notesStatusPanelLastUpdateInfo.visibility = View.GONE
                        bind.notesStatusPanelUnsavedChangesInfo.visibility = View.GONE
                    }
                    is SyncStatus.WaitingForNetwork -> {
                        bind.notesStatusPanelSyncProgressBar.visibility = View.VISIBLE
                        bind.notesStatusPanelLastUpdateInfo.visibility = View.VISIBLE
                        bind.notesStatusPanelUnsavedChangesInfo.visibility = View.GONE

                        bind.notesStatusPanelLastUpdateInfo.text =
                            getString(R.string.notes_statusPanel_waitingForNetwork)
                    }
                    is SyncStatus.Synced -> {
                        bind.notesStatusPanelSyncProgressBar.visibility = View.GONE
                        bind.notesStatusPanelLastUpdateInfo.visibility = View.VISIBLE
                        bind.notesStatusPanelUnsavedChangesInfo.visibility = View.VISIBLE

                        val dateTimeStr = dateFormatHelper.formatDateTime(syncStatus.date)
                        bind.notesStatusPanelLastUpdateInfo.text =
                            if (syncStatus.isError)
                                getString(R.string.notes_statusPanel_lastUpdateInfo_syncError_prefix) + dateTimeStr
                            else
                                getString(R.string.notes_statusPanel_lastUpdateInfo_prefix) + dateTimeStr

                        if (syncStatus.unsavedChangesCount > 0) {
                            val prefix = getString(
                                R.string.notes_statusPanel_unsyncedChangesInfo_prefix
                            )
                            bind.notesStatusPanelUnsavedChangesInfo.text =
                                prefix + syncStatus.unsavedChangesCount

                        } else {
                            bind.notesStatusPanelUnsavedChangesInfo.text =
                                getString(R.string.notes_statusPanel_unsyncedChangesInfo_none)
                        }
                    }
                }
            }
        }
    }
}

