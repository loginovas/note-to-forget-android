package iloginovas.notetoforget.ui.notes

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import iloginovas.notetoforget.data.model.Note
import iloginovas.notetoforget.databinding.NoteItemBinding
import iloginovas.notetoforget.ui.DateFormatHelper
import java.util.*

class NoteHolder(
    private val binding: NoteItemBinding,
    private val dateFormatHelper: DateFormatHelper,
    private val clickListener: (Note) -> Unit
) : RecyclerView.ViewHolder(binding.root), View.OnClickListener {

    private lateinit var note: Note

    init {
        binding.root.setOnClickListener(this)
    }

    fun bind(note: Note) {
        this.note = note

        with(binding.noteItemTitle) {
            if (note.title.isBlank()) {
                visibility = View.GONE
            } else {
                visibility = View.VISIBLE
                text = note.title
            }
        }
        with(binding.noteItemText) {
            if (note.text.isBlank()) {
                visibility = View.GONE
            } else {
                visibility = View.VISIBLE
                text = note.text
            }
        }
        with(binding.noteItemDate) {
            val date = dateFormatHelper.dateFormatMedium.format(note.lastUpdate)
            val time = dateFormatHelper.timeFormat.format(note.lastUpdate)
            text = "$date $time"
        }

        binding.noteItemSyncIcon.visibility =
            if (note.isSyncNeeded) View.VISIBLE else View.GONE
    }

    override fun onClick(v: View?) {
        clickListener(note)
    }
}

class NoteAdapter(
    private val dateFormatHelper: DateFormatHelper,
    private val noteItemClickListener: (note: Note) -> Unit
) : PagingDataAdapter<Note, NoteHolder>(diffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = NoteItemBinding.inflate(inflater, parent, false)
        return NoteHolder(binding, dateFormatHelper, noteItemClickListener)
    }

    override fun onBindViewHolder(holder: NoteHolder, position: Int) {
        val item = getItem(position) ?: Note.new(creationDate = Date(0))
        holder.bind(item)
    }

    companion object {
        private val diffCallback = object : DiffUtil.ItemCallback<Note>() {
            override fun areItemsTheSame(oldItem: Note, newItem: Note): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: Note, newItem: Note): Boolean =
                oldItem == newItem
        }
    }
}