package iloginovas.notetoforget.ui.notes.editor

import android.app.Application
import android.text.Editable
import androidx.annotation.MainThread
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import iloginovas.notetoforget.common.NoteId
import iloginovas.notetoforget.data.model.Note
import iloginovas.notetoforget.ui.common.LoggedInScopeViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import org.koin.core.scope.Scope
import java.util.*

sealed class LastSavedNote {
    object Loading : LastSavedNote()
    class Loaded(val note: Note?) : LastSavedNote()

    fun loadedOrNull(): Note? = when (this) {
        is Loaded -> note
        else -> null
    }
}

class TitleAndText private constructor(val title: String, val text: String) {
    companion object {
        @MainThread
        fun trimAndCreate(title: Editable, text: Editable) =
            TitleAndText(title.toString().trim(), text.toString().trim())
    }

    val areBothEmpty: Boolean
        get() = title.isEmpty() && text.isEmpty()
}

class NoteEditorViewModel(
    private val savedState: SavedStateHandle,
    app: Application,
    koinScope: Scope
) : LoggedInScopeViewModel(app, koinScope) {

    sealed class Event {
        class ShowDialog(val dialogType: OnBackPressedDialogFragment.Type) : Event()
        object NavigateBack : Event()
        sealed class ShowWarning : Event() {
            object CannotSaveNoteBecauseTitleAndTextIsBlank : ShowWarning()
        }
    }

    private var noteId: Long? = savedState[NoteEditorFragment.ARG_NOTE_ID]

    val lastSavedNote = MutableStateFlow<LastSavedNote>(LastSavedNote.Loading)

    private val eventChannel = Channel<Event>(Channel.BUFFERED)
    val events: Flow<Event> = flow {
        while (true) {
            emit(eventChannel.receive())
        }
    }

    init {
        reloadNoteFromDatabaseAsync()
    }

    fun removeNote() {
        val id = noteId ?: return
        viewModelScope.launch {
            getRepository().removeNote(id)
            eventChannel.send(Event.NavigateBack)
        }
    }

    fun saveNote(titleAndText: TitleAndText) {
        viewModelScope.launch {
            if (titleAndText.areBothEmpty) {
                eventChannel.send(Event.ShowWarning.CannotSaveNoteBecauseTitleAndTextIsBlank)
            } else {
                saveNoteInternal(titleAndText)
                reloadNoteFromDatabaseAsync()
            }
        }
    }

    fun saveNoteAndNavigateBack(titleAndText: TitleAndText) {
        viewModelScope.launch {
            if (titleAndText.areBothEmpty) {
                eventChannel.send(Event.ShowWarning.CannotSaveNoteBecauseTitleAndTextIsBlank)
            } else {
                saveNoteInternal(titleAndText)
                eventChannel.send(Event.NavigateBack)
            }
        }
    }

    private suspend fun saveNoteInternal(titleAndText: TitleAndText) {
        val id = noteId
        val currentDate = Date()

        val note = if (id == null) {
            val newId: Long = NoteId.generate(currentDate)
            savedState[NoteEditorFragment.ARG_NOTE_ID] = newId
            noteId = newId
            Note(newId, titleAndText.title, titleAndText.text, currentDate)
        } else {
            Note(id, titleAndText.title, titleAndText.text, currentDate)
        }
        getRepository().putNote(note)
    }

    fun handleOnBackPressed(titleAndText: TitleAndText) {
        val lastSavedNote: Note? = lastSavedNote.value.loadedOrNull()
        val title = titleAndText.title
        val text = titleAndText.text

        val dialogType: OnBackPressedDialogFragment.Type? = when (lastSavedNote) {
            null -> {
                if (titleAndText.areBothEmpty) null
                else OnBackPressedDialogFragment.Type.THERE_ARE_UNSAVED_CHANGES
            }
            else -> {
                if (titleAndText.areBothEmpty) {
                    OnBackPressedDialogFragment.Type.THERE_ARE_UNSAVED_CHANGES_AND_TITLE_WITH_TEXT_ARE_EMPTY
                } else {
                    val areThereUnsavedChanges: Boolean =
                        title != lastSavedNote.title.trim() || text != lastSavedNote.text

                    if (areThereUnsavedChanges)
                        OnBackPressedDialogFragment.Type.THERE_ARE_UNSAVED_CHANGES
                    else null
                }
            }
        }

        viewModelScope.launch {
            if (dialogType == null) {
                eventChannel.send(Event.NavigateBack)
            } else {
                eventChannel.send(Event.ShowDialog(dialogType))
            }
        }
    }

    var isViewInitializedWithLastSavedNote: Boolean
        get() = savedState[IS_VIEW_INITIALIZED_WITH_LAST_SAVED_NOTE] ?: false
        set(value) {
            savedState[IS_VIEW_INITIALIZED_WITH_LAST_SAVED_NOTE] = value
        }

    private fun reloadNoteFromDatabaseAsync() {
        val id = noteId
        if (id == null) {
            lastSavedNote.value = LastSavedNote.Loaded(null)
            return
        }
        viewModelScope.launch(Dispatchers.IO) {
            lastSavedNote.value = LastSavedNote.Loaded(getRepository().getNoteById(id))
        }
    }
}

private const val IS_VIEW_INITIALIZED_WITH_LAST_SAVED_NOTE =
    "savedState_isViewInitializedWithLastSavedNote"