package iloginovas.notetoforget.ui.notes

import android.app.Application
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import iloginovas.notetoforget.data.model.Note
import iloginovas.notetoforget.data.model.SyncStatus
import iloginovas.notetoforget.data.repository.NoteRepository
import iloginovas.notetoforget.ui.common.LoggedInScopeViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import org.koin.core.scope.Scope

class NotesViewModel(
    app: Application,
    scope: Scope
) : LoggedInScopeViewModel(app, scope) {

    sealed class State {
        object Initializing : State()
    }

    inner class ReadyState(private val noteRepository: NoteRepository) : State() {

        val notesFlow: Flow<PagingData<Note>> = noteRepository
            .notePagingDataFlow().cachedIn(viewModelScope)

        val syncStatusFlow: Flow<SyncStatus> = noteRepository.syncStatus()
    }

    val state: StateFlow<State> =
        flow<State> {
            emit(ReadyState(getRepository()))
        }.stateIn(viewModelScope, SharingStarted.Eagerly, State.Initializing)

    fun launchSync() {
        viewModelScope.launch {
            getRepository().launchSync()
        }
    }
}