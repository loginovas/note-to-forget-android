package iloginovas.notetoforget.ui.notes.editor

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import iloginovas.notetoforget.R
import iloginovas.notetoforget.data.model.Note
import iloginovas.notetoforget.ui.DateFormatHelper
import iloginovas.notetoforget.ui.common.LoggedInScopeFragment
import iloginovas.notetoforget.ui.notes.editor.NoteEditorViewModel.Event
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.stateViewModel

class NoteEditorFragment : LoggedInScopeFragment(R.layout.note_editor),
    OnBackPressedDialogFragment.Callback {

    companion object {
        const val ARG_NOTE_ID = "noteId"

        fun createForNewNote() = NoteEditorFragment()

        fun createForNote(noteId: Long) = NoteEditorFragment().apply {
            arguments = Bundle().apply {
                putLong(ARG_NOTE_ID, noteId)
            }
        }
    }

    private val dateFormatHelper: DateFormatHelper by inject()

    private val viewModel: NoteEditorViewModel by stateViewModel(
        state = { arguments ?: Bundle() }
    )

    private lateinit var onBackPressedListener: OnBackPressedListener

    private lateinit var statusPanel: LinearLayout
    private lateinit var createDateLabel: TextView
    private lateinit var createTimeLabel: TextView
    private lateinit var updateDateLabel: TextView
    private lateinit var updateTimeLabel: TextView

    private lateinit var titleTextField: EditText
    private lateinit var noteTextField: EditText

    private val titleAndText: TitleAndText
        get() = TitleAndText.trimAndCreate(titleTextField.text, noteTextField.text)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

        onBackPressedListener = OnBackPressedListener()
        requireActivity().onBackPressedDispatcher.addCallback(this, onBackPressedListener)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.note_editor, menu)

        menu.findItem(R.id.noteEditor_delete_menuItem).apply {
            val deleteActionVisibility =
                when (val state = viewModel.lastSavedNote.value) {
                    is LastSavedNote.Loading -> false
                    is LastSavedNote.Loaded -> state.note != null
                }

            isVisible = deleteActionVisibility
        }
    }

    override fun saveNoteAndNavigateBack() {
        viewModel.saveNoteAndNavigateBack(titleAndText)
    }

    override fun navigateBack() {
        onBackPressedListener.isEnabled = false
        activity?.onBackPressed()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (requireActivity() as AppCompatActivity).supportActionBar?.apply {
            setTitle(R.string.noteEditor_appBarTitle)
            setDisplayHomeAsUpEnabled(true)
        }

        statusPanel = view.findViewById(R.id.noteEditor_statusPanel)
        createDateLabel = view.findViewById(R.id.noteEditor_createDate)
        createTimeLabel = view.findViewById(R.id.noteEditor_createTime)
        updateDateLabel = view.findViewById(R.id.noteEditor_updateDate)
        updateTimeLabel = view.findViewById(R.id.noteEditor_updateTime)

        titleTextField = view.findViewById(R.id.noteEditor_title)
        noteTextField = view.findViewById(R.id.noteEditor_text)

        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.lastSavedNote.collect {
                onNoteSaved(it)
            }
        }

        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.events.collect {
                when (it) {
                    is Event.NavigateBack ->
                        navigateBack()

                    is Event.ShowDialog ->
                        OnBackPressedDialogFragment.create(it.dialogType)
                            .show(childFragmentManager, OnBackPressedDialogFragment.TAG)

                    is Event.ShowWarning.CannotSaveNoteBecauseTitleAndTextIsBlank ->
                        Toast.makeText(
                            requireActivity(),
                            getString(R.string.noteEditor_toastMsg_invalidNote_titleAndTextAreEmpty),
                            Toast.LENGTH_SHORT
                        ).show()
                }
            }
        }
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)

        if (!viewModel.isViewInitializedWithLastSavedNote) {
            viewLifecycleOwner.lifecycleScope.launch {
                val loadedNote = viewModel.lastSavedNote
                    .filter { it is LastSavedNote.Loaded }
                    .first() as LastSavedNote.Loaded

                val note: Note? = loadedNote.note
                titleTextField.setText(note?.title ?: "")
                noteTextField.setText(note?.text ?: "")

                viewModel.isViewInitializedWithLastSavedNote = true
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
        when (item.itemId) {
            R.id.noteEditor_save_menuItem -> {
                viewModel.saveNote(titleAndText)
                true
            }
            R.id.noteEditor_delete_menuItem -> {
                viewModel.removeNote()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }

    private fun onNoteSaved(lastSavedNote: LastSavedNote) {
        val note = if (lastSavedNote is LastSavedNote.Loaded)
            lastSavedNote.note
        else null

        if (note == null) {
            statusPanel.visibility = View.GONE
        } else {
            createDateLabel.text = dateFormatHelper.formatDateLong(note.creationDate)
            createTimeLabel.text = dateFormatHelper.formatTime(note.creationDate)

            updateDateLabel.text = dateFormatHelper.formatDateLong(note.lastUpdate)
            updateTimeLabel.text = dateFormatHelper.formatTime(note.lastUpdate)

            statusPanel.visibility = View.VISIBLE
        }

        requireActivity().invalidateOptionsMenu()
    }

    private inner class OnBackPressedListener : OnBackPressedCallback(true) {
        override fun handleOnBackPressed() {
            viewModel.handleOnBackPressed(titleAndText)
        }
    }
}