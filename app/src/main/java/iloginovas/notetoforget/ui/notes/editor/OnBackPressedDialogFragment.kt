package iloginovas.notetoforget.ui.notes.editor

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import iloginovas.notetoforget.R
import java.lang.IllegalStateException

class OnBackPressedDialogFragment : DialogFragment() {

    interface Callback {
        fun saveNoteAndNavigateBack()
        fun navigateBack()
    }

    enum class Type {
        THERE_ARE_UNSAVED_CHANGES,
        THERE_ARE_UNSAVED_CHANGES_AND_TITLE_WITH_TEXT_ARE_EMPTY
    }

    companion object {
        private const val ARG_DIALOG_TYPE = "dialogType"
        const val TAG = "OnBackPressedDialogFragment"

        fun create(type: Type) = OnBackPressedDialogFragment().apply {
            arguments = Bundle().apply {
                putInt(ARG_DIALOG_TYPE, type.ordinal)
            }
        }
    }

    private lateinit var type: Type
    private lateinit var callback: Callback

    override fun onAttach(context: Context) {
        super.onAttach(context)

        val typeOrdinal = requireArguments().getInt(ARG_DIALOG_TYPE)
        type = Type.values()[typeOrdinal]
        callback = parentFragment as Callback
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)

            when (type) {
                Type.THERE_ARE_UNSAVED_CHANGES ->
                    builder
                        .setMessage(getString(R.string.noteEditor_unsavedChangesDialog_msg))
                        .setPositiveButton(getString(R.string.noteEditor_unsavedChangesDialog_button_yes)) { _, _ ->
                            callback.saveNoteAndNavigateBack()
                        }
                        .setNegativeButton(getString(R.string.noteEditor_unsavedChangesDialog_button_no)) { _, _ ->
                            callback.navigateBack()
                        }
                        .setNeutralButton(getString(R.string.noteEditor_unsavedChangesDialog_button_cancel)) { _, _ ->
                            dismiss()
                        }
                Type.THERE_ARE_UNSAVED_CHANGES_AND_TITLE_WITH_TEXT_ARE_EMPTY ->
                    builder
                        .setMessage("Title and text are not specified. Leave without saving?")
                        .setPositiveButton(getString(R.string.noteEditor_unsavedChangesDialog_button_yes)) { _, _ ->
                            callback.navigateBack()
                        }
                        .setNeutralButton(getString(R.string.noteEditor_unsavedChangesDialog_button_cancel)) { _, _ ->
                            dismiss()
                        }
            }

            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }
}