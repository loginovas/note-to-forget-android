package iloginovas.notetoforget.service

import iloginovas.notetoforget.common.Api
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST

interface LoginService {

    @POST("user/login")
    suspend fun login(@Body credentials: Api.UserCredentials): Response<Api.LoginResponse>

    @POST("user/logout")
    suspend fun logout(@Header("SESSION") sessionId: String): Void

    @POST("user/registration")
    suspend fun register(@Body credentials: Api.UserCredentials): Response<Api.LoginResponse>
}