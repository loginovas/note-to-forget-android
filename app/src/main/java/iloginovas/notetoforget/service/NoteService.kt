package iloginovas.notetoforget.service

import iloginovas.notetoforget.common.Api
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.POST

interface NoteService {

    @POST("sync")
    @Headers(
        "Accept: application/json",
        "Content-Type: application/json; charset=UTF-8"
    )
    suspend fun sync(
        @Header("SESSION") sessionId: String,
        @Body changes: Api.NoteChanges
    ): Api.NoteChanges
}