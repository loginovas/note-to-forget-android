package iloginovas.notetoforget.service

import iloginovas.notetoforget.common.Api
import iloginovas.notetoforget.common.error
import iloginovas.notetoforget.data.LoginResult
import iloginovas.notetoforget.data.model.Note
import iloginovas.notetoforget.data.model.User
import iloginovas.notetoforget.data.model.UserCredentials
import java.lang.IllegalStateException
import java.util.*

fun Api.User.toModel() = User(id, name)

fun UserCredentials.fromModel() = Api.UserCredentials(username, password)

fun Api.LoginResponse.toModel(): LoginResult {
    onSuccess?.let {
        return LoginResult.Successful(it.user.toModel(), it.sessionId)
    }
    onFailure?.let {
        return LoginResult.Failed(it.error)
    }
    throw IllegalStateException()
}
