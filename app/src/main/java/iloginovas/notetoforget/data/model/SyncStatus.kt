package iloginovas.notetoforget.data.model

import java.util.*

sealed class SyncStatus {
    object NeverSynced : SyncStatus()
    object InProgress : SyncStatus()
    object WaitingForNetwork : SyncStatus()
    class Synced(
        val date: Date,
        val isError: Boolean,
        val unsavedChangesCount: Int
    ) : SyncStatus()
}