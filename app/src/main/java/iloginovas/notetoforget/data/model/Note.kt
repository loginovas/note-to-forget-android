package iloginovas.notetoforget.data.model

import iloginovas.notetoforget.common.NoteId
import java.util.*

data class Note(
    /** @see NoteId */
    val id: Long,
    val title: String = "",
    val text: String = "",
    val lastUpdate: Date,
    val isSyncNeeded: Boolean = true
) {

    val creationDate: Date by lazy {
        Date(NoteId.getMillisSinceEpoch(id))
    }

    companion object {
        fun new(creationDate: Date, title: String = "", text: String = ""): Note {
            val id: Long = NoteId.generate(creationDate)
            return Note(id, title, text, creationDate, true)
        }
    }
}