package iloginovas.notetoforget.data.model

class UserCredentials(
    val username: String,
    val password: String
)