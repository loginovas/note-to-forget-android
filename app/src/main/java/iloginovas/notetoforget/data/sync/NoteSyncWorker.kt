package iloginovas.notetoforget.data.sync

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.work.*
import iloginovas.notetoforget.App
import iloginovas.notetoforget.R
import iloginovas.notetoforget.data.UserSession
import iloginovas.notetoforget.data.getOrCreateUserSessionScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber

class NoteSyncWorker(
    appContext: Context,
    workerParams: WorkerParameters
) : CoroutineWorker(appContext, workerParams) {

    companion object {
        const val WORK_TAG = "NoteSyncWorker"
        const val FOREGROUND_NOTIFICATION_ID = 1

        const val USER_ID_PARAMETER = "userId"
        const val SESSION_ID_PARAMETER = "sessionId"

        fun uniqueWorkName(userId: Int): String {
            return "noteSync_$userId"
        }

        fun createRequest(userSession: UserSession): OneTimeWorkRequest {
            return OneTimeWorkRequestBuilder<NoteSyncWorker>()
                .addTag(WORK_TAG)
                .setInputData(
                    workDataOf(
                        USER_ID_PARAMETER to userSession.userId,
                        SESSION_ID_PARAMETER to userSession.sessionId
                    )
                )
                .setConstraints(
                    Constraints.Builder()
                        .setRequiredNetworkType(NetworkType.CONNECTED)
                        .build()
                )
                .build()
        }
    }

    override suspend fun doWork(): Result {
        setForeground(createForegroundInfo())
        return withContext(Dispatchers.IO) {
            try {
                doWorkInternal()
            } catch (e: Throwable) {
                Timber.d(e)
                Result.failure()
            }
        }
    }

    // It is not private because the bug https://youtrack.jetbrains.com/issue/KT-27810
    // that do not allow to debug private suspend functions.
    @Suppress("MemberVisibilityCanBePrivate")
    suspend fun doWorkInternal(): Result {
        val app: App = applicationContext as? App
            ?: return Result.failure()

        val userSession = with(inputData.keyValueMap) {
            UserSession(
                get(USER_ID_PARAMETER) as Int,
                get(SESSION_ID_PARAMETER) as String
            )
        }

        val userScope = app.koin.getOrCreateUserSessionScope(userSession)
        val noteSyncLogic: NoteSyncLogic = userScope.get()
        noteSyncLogic.sync()

        return Result.success()
    }

    private fun createForegroundInfo(): ForegroundInfo {
        val channelId = applicationContext.getString(R.string.noteSyncNotification_channel_id)
        val title = applicationContext.getString(R.string.noteSyncNotification_title)
        val cancel = applicationContext.getString(R.string.noteSyncNotification_cancel)

        val cancelIntent =
            WorkManager.getInstance(applicationContext).createCancelPendingIntent(id)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel()
        }

        val notification: Notification = NotificationCompat.Builder(applicationContext, channelId)
            .setContentTitle(title)
            .setTicker(title)
            .setSmallIcon(R.drawable.outline_sync_24)
            .setOngoing(true)
            .addAction(android.R.drawable.ic_delete, cancel, cancelIntent)
            .build()

        return ForegroundInfo(FOREGROUND_NOTIFICATION_ID, notification)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel() {
        val id = applicationContext.getString(R.string.noteSyncNotification_channel_id)
        val name = applicationContext.getString(R.string.noteSyncNotification_channel_name)
        val description =
            applicationContext.getString(R.string.noteSyncNotification_channel_description)
        val importance = NotificationManager.IMPORTANCE_DEFAULT

        val notificationChannel = NotificationChannel(id, name, importance)
        notificationChannel.description = description

        val notificationManager = applicationContext.getSystemService(Context.NOTIFICATION_SERVICE)
                as NotificationManager
        notificationManager.createNotificationChannel(notificationChannel)
    }
}