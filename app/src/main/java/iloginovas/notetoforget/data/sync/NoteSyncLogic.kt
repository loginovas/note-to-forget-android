package iloginovas.notetoforget.data.sync

import androidx.room.withTransaction
import iloginovas.notetoforget.common.Api
import iloginovas.notetoforget.data.UserSession
import iloginovas.notetoforget.data.db.*
import iloginovas.notetoforget.service.NoteService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.*

class NoteSyncLogic(
    private val userSession: UserSession,
    private val db: AppDatabase,
    private val keyValueStorage: KeyValueStorage,
    private val noteService: NoteService
) {

    companion object {
        const val NEVER_SYNCED = -1L
    }

    suspend fun sync() {
        // We set the error flag because if an exception will be thrown or the coroutine
        // will be cancelled then the status info will be lost. If the sync is finished
        // successfully then status is updated at the end of this operation.
        db.withTransaction {
            keyValueStorage.put(LAST_SYNC_ERROR_FLAG, true)
            keyValueStorage.put(LAST_SYNC_DATE, Date().time)
        }

        val noteChangeDao = db.noteChangeDao()
        val inSyncNoteChangeDao = db.inSyncNoteChangeDao()

        val (localChanges: List<NoteChangeWithContent>, latestSyncVersion: Long?) =
            db.withTransaction {
                inSyncNoteChangeDao.insertAllFromNoteChangeTable()
                noteChangeDao.deleteAll()

                val latestSyncVersion: Long? = keyValueStorage.get(LAST_SYNC_VERSION)
                inSyncNoteChangeDao.getNoteChangesWithContent() to latestSyncVersion
            }

        val noteChanges: Api.NoteChanges = withContext(Dispatchers.Default) {
            val notesToBeRemoved: List<Long> = localChanges.asSequence()
                .filter { it.noteChange.type == NoteChangeLog.Type.DELETE }
                .map { it.noteChange.noteId }
                .toList()

            val notesToBePut: List<Api.Note> = localChanges.asSequence()
                .filter { it.noteChange.type == NoteChangeLog.Type.PUT }
                .map {
                    val note: NoteEntity = it.note!!
                    val lastUpdate: Long = note.updatedAt.time / 1000
                    Api.Note(note.id, note.title, note.text, lastUpdate)
                }
                .toList()

            Api.NoteChanges(
                latestSyncVersion ?: NEVER_SYNCED,
                notesToBeRemoved,
                notesToBePut
            )
        }

        val syncResult: Api.NoteChanges = noteService.sync(userSession.sessionId, noteChanges)

        val noteDao = db.noteDao()
        db.withTransaction {
            // Changes made by the user while syncing was in progress.
            // These changes have a higher priority over those in [syncResult].
            val newLocalChanges: Map<Long, NoteChange> = noteChangeDao.getAll()
                .associateBy { it.noteId }

            syncResult.notesToBeRemoved.forEach {
                if (!newLocalChanges.containsKey(it))
                    noteDao.delete(it)
            }

            syncResult.notesToBePut.forEach {
                if (newLocalChanges.containsKey(it.id))
                    return@forEach

                noteDao.insertOrReplace(
                    NoteEntity(
                        it.id, it.title, it.text,
                        updatedAt = Date(it.lastUpdate * 1000),
                        isSyncNeeded = false
                    )
                )
            }

            // Reset isSyncNeeded flag
            localChanges.forEach {
                val noteId = it.noteChange.noteId
                if (!newLocalChanges.containsKey(noteId)) {
                    noteDao.updateSyncFlag(noteId, false)
                }
            }

            keyValueStorage.put(LAST_SYNC_VERSION, syncResult.version)
            inSyncNoteChangeDao.deleteAll()

            // Update sync status
            keyValueStorage.put(LAST_SYNC_ERROR_FLAG, false)
            keyValueStorage.put(LAST_SYNC_DATE, Date().time)
        }
    }
}