package iloginovas.notetoforget.data.db

import androidx.paging.PagingSource
import androidx.room.*

@Dao
interface NoteDao {

    @Query("SELECT * FROM note ORDER BY id DESC")
    fun pagingSource(): PagingSource<Int, NoteEntity>

    @Query("SELECT * FROM note WHERE id=:id")
    suspend fun getById(id: Long): NoteEntity?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertOrReplace(note: NoteEntity)

    @Query("DELETE FROM note WHERE id=:noteId")
    suspend fun delete(noteId: Long)

    @Query("UPDATE note SET isSyncNeeded=:isSyncNeeded WHERE id=:noteId")
    suspend fun updateSyncFlag(noteId: Long, isSyncNeeded: Boolean)
}