package iloginovas.notetoforget.data.db

import androidx.room.TypeConverter
import java.util.*

@Suppress("unused")
class DbTypeConverter {

    @TypeConverter
    fun fromDate(date: Date?): Long? = when (date) {
        null -> null
        else -> date.time / 1000
    }

    @TypeConverter
    fun toDate(secondsSinceEpoch: Long?): Date? =
        secondsSinceEpoch?.let { Date(it * 1000) }

    @TypeConverter
    fun fromUUID(uuid: UUID): String = uuid.toString()

    @TypeConverter
    fun toUUID(stringUUID: String): UUID = UUID.fromString(stringUUID)

    @TypeConverter
    fun fromChangeRecordType(type: NoteChangeLog.Type): Int = type.id

    private val noteChangeTypeById: Map<Int, NoteChangeLog.Type> =
        NoteChangeLog.Type.values().associateBy { it.id }

    @TypeConverter
    fun toChangeRecordType(recordTypeId: Int): NoteChangeLog.Type =
        noteChangeTypeById[recordTypeId]!!

}