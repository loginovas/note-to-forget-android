package iloginovas.notetoforget.data.db

import androidx.room.Entity
import androidx.room.PrimaryKey

open class NoteChangeLog(
    @PrimaryKey(autoGenerate = false)
    val noteId: Long,
    val type: Type
) {
    enum class Type(val id: Int) {
        PUT(0),
        DELETE(2)
    }
}


@Entity(tableName = "note_change")
class NoteChange(
    noteId: Long,
    type: Type
) : NoteChangeLog(noteId, type)


@Entity(tableName = "in_sync_note_change")
class InSyncNoteChange(
    noteId: Long,
    type: Type
) : NoteChangeLog(noteId, type)