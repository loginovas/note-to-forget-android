package iloginovas.notetoforget.data.db

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "note")
data class NoteEntity(

    /**
     * Time ordered id. Values ordered by creation date.
     * See [iloginovas.notetoforget.common.NoteId]
     */
    @PrimaryKey
    val id: Long,

    val title: String,

    val text: String,

    /** Stored as seconds since epoch */
    val updatedAt: Date,

    val isSyncNeeded: Boolean
)