package iloginovas.notetoforget.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase

@Database(
    entities = [
        NoteEntity::class, NoteChange::class, InSyncNoteChange::class
    ],
    version = 1
)
@TypeConverters(DbTypeConverter::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun noteDao(): NoteDao
    abstract fun noteChangeDao(): NoteChangeDao
    abstract fun inSyncNoteChangeDao(): InSyncNoteChangeDao

    companion object {

        fun deleteDatabase(context: Context, userId: Int) {
            context.deleteDatabase(databaseName(userId))
        }

        fun create(context: Context, userId: Int): AppDatabase {
            return Room
                .databaseBuilder(context, AppDatabase::class.java, databaseName(userId))
                .configure()
                .build()
        }

        fun createInMemory(context: Context): AppDatabase {
            return Room
                .inMemoryDatabaseBuilder(context, AppDatabase::class.java)
                .configure()
                .build()
        }

        private fun Builder<AppDatabase>.configure(): Builder<AppDatabase> =
            apply {
                addMigrations()
                addCallback(RoomDbCallback())
            }

        private fun databaseName(userId: Int) = "note-$userId.db"
    }
}

private class RoomDbCallback : RoomDatabase.Callback() {
    override fun onCreate(db: SupportSQLiteDatabase) {
        KeyValueStorageImpl.createTable(db)
    }
}