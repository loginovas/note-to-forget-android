package iloginovas.notetoforget.data.db

import android.content.ContentValues
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import iloginovas.notetoforget.BuildConfig
import timber.log.Timber
import java.io.Serializable
import kotlin.reflect.KClass
import kotlin.reflect.KFunction2

const val LAST_SYNC_VERSION = "last_sync_version"
const val LAST_SYNC_DATE = "last_sync_date" // in ms
const val LAST_SYNC_ERROR_FLAG = "last_sync_error_flag"

/**
 * Allows to store key-value data in SQLite database.
 */
interface KeyValueStorage {

    fun <T : Any> get(key: String, type: KClass<T>): T?

    fun remove(key: String) {
        put(key, null)
    }

    fun <T> put(key: String, value: T?)
}

inline fun <reified T : Any> KeyValueStorage.get(key: String): T? =
    get(key, T::class)


@Suppress("ConvertTryFinallyToUseCall")
class KeyValueStorageImpl(private val database: AppDatabase) : KeyValueStorage {

    companion object {
        private const val TABLE_NAME = "key_value_storage"
        private const val COLUMN_KEY = "key"
        private const val COLUMN_VALUE = "value"

        private const val SELECT_STATEMENT = "SELECT * FROM $TABLE_NAME WHERE $COLUMN_KEY=? LIMIT 1"
        private const val DELETE_STATEMENT = "DELETE FROM $TABLE_NAME WHERE $COLUMN_KEY=?"

        fun createTable(database: SupportSQLiteDatabase) {
            database.execSQL(
                "CREATE TABLE $TABLE_NAME (" +
                        "$COLUMN_KEY TEXT PRIMARY KEY," +
                        "$COLUMN_VALUE)"
            )
        }
    }

    @Suppress("UNCHECKED_CAST")
    override fun <T : Any> get(key: String, type: KClass<T>): T? {

        fun Cursor.getBoolean(column: Int): Boolean =
            getString(column).toBoolean()

        val valueGetter: KFunction2<Cursor, Int, Serializable> = when (type) {
            Int::class -> Cursor::getInt
            Long::class -> Cursor::getLong
            Float::class -> Cursor::getFloat
            Double::class -> Cursor::getDouble
            Short::class -> Cursor::getShort
            ByteArray::class -> Cursor::getBlob
            String::class -> Cursor::getString
            Boolean::class -> Cursor::getBoolean
            else -> throw IllegalArgumentException()
        }
        return get(key, valueGetter) as T?
    }

    fun <T : Any> get(
        key: String,
        getValue: Cursor.(column: Int) -> T?
    ): T? {
        val cursor = database.query(SELECT_STATEMENT, arrayOf(key))
        return try {
            val valueColumnIndex = cursor.getColumnIndexOrThrow(COLUMN_VALUE)
            if (cursor.moveToNext()) {
                cursor.getValue(valueColumnIndex)
            } else {
                null
            }
        } catch (e: Throwable) {
            if (BuildConfig.DEBUG) throw e
            Timber.d(e)
            null
        } finally {
            cursor.close()
        }
    }

    override fun <T> put(key: String, value: T?) {
        if (value == null) {
            put(key, null)
            return
        }

        val contentValues = ContentValues()
        contentValues.put(COLUMN_KEY, key)

        when (value) {
            is String -> contentValues.put(COLUMN_VALUE, value)
            is Short -> contentValues.put(COLUMN_VALUE, value)
            is Int -> contentValues.put(COLUMN_VALUE, value)
            is Long -> contentValues.put(COLUMN_VALUE, value)
            is Float -> contentValues.put(COLUMN_VALUE, value)
            is Double -> contentValues.put(COLUMN_VALUE, value)
            is Boolean -> contentValues.put(COLUMN_VALUE, value.toString())
            is ByteArray -> contentValues.put(COLUMN_VALUE, value)
            else -> throw IllegalArgumentException()
        }
        put(key, contentValues)
    }

    private fun put(
        key: String,
        keyValuePair: ContentValues?
    ) {
        try {
            val db = database.openHelper.writableDatabase
            if (keyValuePair == null) {
                db.delete(TABLE_NAME, "$COLUMN_KEY=?", arrayOf(key))
            } else {
                db.insert(TABLE_NAME, SQLiteDatabase.CONFLICT_REPLACE, keyValuePair)
            }
        } catch (e: Throwable) {
            if (BuildConfig.DEBUG) throw e
            Timber.d(e)
        }
    }
}