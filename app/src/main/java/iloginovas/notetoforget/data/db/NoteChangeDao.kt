package iloginovas.notetoforget.data.db

import androidx.room.*
import kotlinx.coroutines.flow.Flow

@Dao
interface NoteChangeDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(noteChange: NoteChange)

    @Query("SELECT * FROM note_change")
    suspend fun getAll(): List<NoteChange>

    @Query("DELETE FROM note_change")
    suspend fun deleteAll()

    @Transaction
    @Query(
        "SELECT count(*) FROM( " +
                "SELECT noteId FROM note_change " +
                "UNION " +
                "SELECT noteId FROM in_sync_note_change" + ")"
    )
    fun getTotalChangeCount(): Flow<Int>
}

@Dao
interface InSyncNoteChangeDao {

    @Query(
        "INSERT OR REPLACE INTO in_sync_note_change " +
                "SELECT * FROM note_change"
    )
    suspend fun insertAllFromNoteChangeTable()

    @Transaction
    @Query("SELECT * FROM in_sync_note_change")
    suspend fun getNoteChangesWithContent(): List<NoteChangeWithContent>

    @Query("DELETE FROM in_sync_note_change")
    suspend fun deleteAll()
}

class NoteChangeWithContent(
    @Embedded
    val noteChange: InSyncNoteChange,

    @Relation(parentColumn = "noteId", entityColumn = "id")
    val note: NoteEntity?
)