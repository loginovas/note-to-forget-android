package iloginovas.notetoforget.data

import org.koin.core.Koin
import org.koin.core.qualifier.TypeQualifier
import org.koin.core.scope.Scope

data class UserSession(val userId: Int, val sessionId: String) {

    /** Koin scope id */
    val scopeId: String get() = userId.toString()
}

fun Koin.getOrCreateUserSessionScope(userSession: UserSession): Scope =
    getOrCreateScope(userSession.scopeId, TypeQualifier(UserSession::class), userSession)