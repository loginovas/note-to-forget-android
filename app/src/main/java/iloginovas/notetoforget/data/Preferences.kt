package iloginovas.notetoforget.data

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit
import androidx.preference.PreferenceManager
import com.google.gson.Gson
import timber.log.Timber

interface Preferences {
    val loginScreenRecentUsername: Pref<String>
    val userSession: Pref<UserSession>
    val username: Pref<String>
}

interface Pref<T> {
    var value: T?

    fun remove() {
        value = null
    }
}

class PreferencesImpl(context: Context, private val gson: Gson) : Preferences {

    override val loginScreenRecentUsername = stringPref("loginRecentUsername")
    override val userSession = jsonPref<UserSession>("userSession")
    override val username: Pref<String> = stringPref("username")

    // ========================================================================================
    private val sharedPrefs: SharedPreferences =
        PreferenceManager.getDefaultSharedPreferences(context)

    open inner class SharedPref<T>(
        private val getter: (SharedPreferences) -> T?,
        private val setter: (SharedPreferences.Editor, T?) -> Unit
    ) : Pref<T> {
        override var value: T?
            get() = getter(sharedPrefs)
            set(value) = sharedPrefs.edit { setter(this, value) }
    }

    private fun stringPref(key: String): Pref<String> {
        return SharedPref(
            getter = { sharedPrefs -> sharedPrefs.getString(key, null) },
            setter = { sharedPrefs, value -> sharedPrefs.putString(key, value) }
        )
    }

    private inline fun <reified T> jsonPref(key: String): Pref<T> {
        return SharedPref(
            getter = { sharedPrefs ->
                sharedPrefs.getString(key, null)?.fromJson()
            },
            setter = { sharedPrefs, value ->
                val json = when (value) {
                    null -> null
                    else -> gson.toJson(value)
                }
                sharedPrefs.putString(key, json)
            }
        )
    }

    private inline fun <reified T> String.fromJson(): T? =
        runCatching { gson.fromJson(this, T::class.java) }
            .onFailure { Timber.e(it) }
            .getOrNull()
}