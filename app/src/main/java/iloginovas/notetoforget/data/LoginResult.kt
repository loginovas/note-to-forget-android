package iloginovas.notetoforget.data

import iloginovas.notetoforget.common.login.LoginErrorCause
import iloginovas.notetoforget.data.model.User

sealed class LoginResult {
    class Successful(val user: User, val sessionId: String) : LoginResult()
    class Failed(val errorCause: LoginErrorCause) : LoginResult()
}