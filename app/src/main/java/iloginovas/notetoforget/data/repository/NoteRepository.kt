package iloginovas.notetoforget.data.repository

import androidx.paging.PagingData
import iloginovas.notetoforget.data.model.Note
import iloginovas.notetoforget.data.model.SyncStatus
import kotlinx.coroutines.flow.Flow

interface NoteRepository {

    fun notePagingDataFlow(): Flow<PagingData<Note>>

    suspend fun putNote(note: Note)

    suspend fun getNoteById(noteId: Long): Note?

    suspend fun removeNote(noteId: Long)

    fun launchSync()

    fun syncStatus(): Flow<SyncStatus>
}