package iloginovas.notetoforget.data.repository

import iloginovas.notetoforget.common.Api
import iloginovas.notetoforget.common.login.InvalidFormat
import iloginovas.notetoforget.common.validation.PasswordErrorCause
import iloginovas.notetoforget.common.validation.PasswordValidator
import iloginovas.notetoforget.common.validation.UsernameErrorCause
import iloginovas.notetoforget.common.validation.UsernameValidator
import iloginovas.notetoforget.data.LoginResult
import iloginovas.notetoforget.data.model.UserCredentials
import iloginovas.notetoforget.service.LoginService
import iloginovas.notetoforget.service.fromModel
import iloginovas.notetoforget.service.toModel
import okhttp3.ResponseBody
import retrofit2.Converter
import retrofit2.Response
import retrofit2.Retrofit

class LoginRepositoryImpl(
    private val loginService: LoginService,
    private val retrofit: Retrofit
) : LoginRepository {

    private val loginResponseConverter: Converter<ResponseBody, Api.LoginResponse> by lazy {
        retrofit.responseBodyConverter(Api.LoginResponse::class.java, arrayOf())
    }

    override suspend fun login(userCredentials: UserCredentials): LoginResult {
        validate(userCredentials).let {
            val (usernameError, passwordError) = it
            if (usernameError != null || passwordError != null) {
                return LoginResult.Failed(InvalidFormat(usernameError, passwordError))
            }
        }
        val response = loginService.login(userCredentials.fromModel())
        return response.toResult()
    }

    override suspend fun register(userCredentials: UserCredentials): LoginResult {
        validate(userCredentials).let {
            val (usernameError, passwordError) = it
            if (usernameError != null || passwordError != null) {
                return LoginResult.Failed(InvalidFormat(usernameError, passwordError))
            }
        }
        val response = loginService.register(userCredentials.fromModel())
        return response.toResult()
    }

    private fun Response<Api.LoginResponse>.toResult(): LoginResult {
        val loginResponse: Api.LoginResponse =
            if (isSuccessful) {
                body()!!
            } else errorBody().let {
                if (it == null) throw IllegalStateException("Server error")
                loginResponseConverter.convert(it)!!
            }
        return loginResponse.toModel()
    }
}

private fun validate(
    userCredentials: UserCredentials
): Pair<UsernameErrorCause?, PasswordErrorCause?> {

    val usernameError: UsernameErrorCause? = UsernameValidator.validate(
        userCredentials.username.trim()
    )
    val passwordError: PasswordErrorCause? = PasswordValidator.validate(
        userCredentials.password.trim()
    )
    return Pair(usernameError, passwordError)
}

