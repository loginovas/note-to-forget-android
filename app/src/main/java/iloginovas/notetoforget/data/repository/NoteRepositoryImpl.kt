package iloginovas.notetoforget.data.repository

import android.content.Context
import androidx.lifecycle.asFlow
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.map
import androidx.room.withTransaction
import androidx.work.ExistingWorkPolicy
import androidx.work.WorkInfo
import androidx.work.WorkManager
import iloginovas.notetoforget.data.UserSession
import iloginovas.notetoforget.data.db.*
import iloginovas.notetoforget.data.model.Note
import iloginovas.notetoforget.data.model.SyncStatus
import iloginovas.notetoforget.data.sync.NoteSyncWorker
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import java.util.*

class NoteRepositoryImpl(
    private val appContext: Context,
    private val userSession: UserSession,
    private val keyValueStorage: KeyValueStorage,
    private val db: AppDatabase
) : NoteRepository {

    private val noteDao: NoteDao = db.noteDao()

    override fun notePagingDataFlow(): Flow<PagingData<Note>> {
        return Pager(
            PagingConfig(pageSize = 20),
            pagingSourceFactory = { noteDao.pagingSource() }
        ).flow.map { pagingData: PagingData<NoteEntity> ->
            pagingData.map { it.toModel() }
        }
    }

    override suspend fun putNote(note: Note) {
        db.withTransaction {
            val noteChangeInfo = NoteChange(note.id, NoteChangeLog.Type.PUT)
            db.noteChangeDao().insert(noteChangeInfo)

            val noteEntity = note.fromModel().copy(isSyncNeeded = true)
            noteDao.insertOrReplace(noteEntity)
        }
    }

    override fun launchSync() {
        WorkManager.getInstance(appContext).enqueueUniqueWork(
            NoteSyncWorker.uniqueWorkName(userSession.userId),
            ExistingWorkPolicy.KEEP,
            NoteSyncWorker.createRequest(userSession)
        )
    }

    override fun syncStatus(): Flow<SyncStatus> {
        val uniqueWorkName = NoteSyncWorker.uniqueWorkName(userSession.userId)

        val syncWorkInfoFlow: Flow<WorkInfo?> = WorkManager.getInstance(appContext)
            .getWorkInfosForUniqueWorkLiveData(uniqueWorkName)
            .asFlow()
            .map { it.firstOrNull() }

        val totalChangeCountFlow: Flow<Int> = db.noteChangeDao().getTotalChangeCount()

        val syncStatusFlow: Flow<SyncStatus> = combine(
            syncWorkInfoFlow,
            totalChangeCountFlow
        ) { syncWorkInfo: WorkInfo?, totalChangeCount: Int ->

            // If sync is in progress
            if (syncWorkInfo != null && !syncWorkInfo.state.isFinished) {
                return@combine if (syncWorkInfo.state == WorkInfo.State.ENQUEUED)
                    SyncStatus.WaitingForNetwork
                else
                    SyncStatus.InProgress
            }

            val (syncDate: Date?, isError: Boolean) =
                db.withTransaction {
                    val time: Long? = keyValueStorage.get(LAST_SYNC_DATE)
                    val errorFlag: Boolean = keyValueStorage.get(LAST_SYNC_ERROR_FLAG) ?: false
                    Pair(
                        if (time != null) Date(time) else null,
                        errorFlag
                    )
                }

            if (syncDate == null) {
                SyncStatus.NeverSynced
            } else {
                SyncStatus.Synced(syncDate, isError, totalChangeCount)
            }
        }

        return syncStatusFlow.flowOn(Dispatchers.IO)
    }

    override suspend fun getNoteById(noteId: Long): Note? {
        return noteDao.getById(noteId)?.toModel()
    }

    override suspend fun removeNote(noteId: Long) {
        db.withTransaction {
            val noteChangeInfo = NoteChange(noteId, NoteChangeLog.Type.DELETE)
            db.noteChangeDao().insert(noteChangeInfo)

            noteDao.delete(noteId)
        }
    }
}

private fun NoteEntity.toModel() =
    Note(id, title, text, updatedAt, isSyncNeeded)

private fun Note.fromModel() =
    NoteEntity(id, title, text, lastUpdate, isSyncNeeded)