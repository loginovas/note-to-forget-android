package iloginovas.notetoforget.data.repository

import iloginovas.notetoforget.data.model.UserCredentials
import iloginovas.notetoforget.data.LoginResult

interface LoginRepository {
    suspend fun login(userCredentials: UserCredentials): LoginResult
    suspend fun register(userCredentials: UserCredentials): LoginResult
}