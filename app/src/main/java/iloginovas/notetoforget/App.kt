package iloginovas.notetoforget

import android.app.Application
import iloginovas.notetoforget.DI.COMMON_DEPENDENCIES
import iloginovas.notetoforget.DI.LOGIN_SCREEN_DEPENDENCIES
import iloginovas.notetoforget.DI.NETWORK_DEPENDENCIES
import iloginovas.notetoforget.DI.LOGGED_IN_SCREEN_DEPENDENCIES
import kotlinx.coroutines.*
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.Koin
import org.koin.core.KoinApplication
import org.koin.core.context.GlobalContext
import org.koin.core.context.startKoin
import timber.log.Timber
import java.util.concurrent.atomic.AtomicReference

class App : Application() {

    companion object {
        const val GLOBAL_TAG_RREFIX = "NoteToForger__"

        val koin: Koin
            get() = GlobalContext.get()
    }

    private val koinApplication = AtomicReference<KoinApplication?>(null)

    val koin: Koin get() = koinApplication.get()!!.koin

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(object : Timber.DebugTree() {
                override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
                    super.log(priority, "$GLOBAL_TAG_RREFIX$tag", message, t)
                }
            })
        }

        val koinApp = startKoin {
            androidLogger()
            androidContext(this@App)
            modules(
                COMMON_DEPENDENCIES,
                NETWORK_DEPENDENCIES,
                LOGIN_SCREEN_DEPENDENCIES,
                LOGGED_IN_SCREEN_DEPENDENCIES
            )
        }
        koinApplication.set(koinApp)
    }
}

