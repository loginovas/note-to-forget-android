@file:Suppress("RemoveExplicitTypeArguments")

package iloginovas.notetoforget

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import iloginovas.notetoforget.data.Preferences
import iloginovas.notetoforget.data.PreferencesImpl
import iloginovas.notetoforget.data.UserSession
import iloginovas.notetoforget.data.db.AppDatabase
import iloginovas.notetoforget.data.db.KeyValueStorage
import iloginovas.notetoforget.data.db.KeyValueStorageImpl
import iloginovas.notetoforget.data.repository.LoginRepository
import iloginovas.notetoforget.data.repository.LoginRepositoryImpl
import iloginovas.notetoforget.data.repository.NoteRepository
import iloginovas.notetoforget.data.repository.NoteRepositoryImpl
import iloginovas.notetoforget.data.sync.NoteSyncLogic
import iloginovas.notetoforget.service.LoginService
import iloginovas.notetoforget.service.NoteService
import iloginovas.notetoforget.ui.DateFormatHelper
import iloginovas.notetoforget.ui.MainFragmentViewModel
import iloginovas.notetoforget.ui.login.LoginViewModel
import iloginovas.notetoforget.ui.notes.editor.NoteEditorViewModel
import iloginovas.notetoforget.ui.notes.NotesViewModel
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

private const val host = "94.19.253.5"
private const val port = 8085

object DI {

    val COMMON_DEPENDENCIES = module {
        single<Gson> { GsonBuilder().setPrettyPrinting().create() }
        single { DateFormatHelper(androidApplication()) }
        single<Preferences> { PreferencesImpl(androidApplication(), get()) }
    }

    val NETWORK_DEPENDENCIES = module {
        single<OkHttpClient> {
            OkHttpClient.Builder()
                .addInterceptor(HttpLoggingInterceptor().apply {
                    level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY
                    else HttpLoggingInterceptor.Level.BASIC
                })
                .readTimeout(Int.MAX_VALUE.toLong(), TimeUnit.MILLISECONDS)
                .build()
        }
        single<Retrofit> {
            Retrofit.Builder()
                .baseUrl("http://$host:$port/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(get())
                .build()
        }
    }

    val LOGIN_SCREEN_DEPENDENCIES = module {
        factory<LoginService> {
            get<Retrofit>().create(LoginService::class.java)
        }
        factory<LoginRepository> { LoginRepositoryImpl(get(), get()) }
        viewModel { LoginViewModel(androidApplication(), get(), get()) }
    }

    val LOGGED_IN_SCREEN_DEPENDENCIES = module {
        scope<UserSession> {
            scoped {
                val userSession = getSource<UserSession>()
                AppDatabase.create(androidApplication(), userSession.userId)
            }

            scoped<KeyValueStorage> { KeyValueStorageImpl(get()) }
            scoped<NoteService> { get<Retrofit>().create(NoteService::class.java) }
            scoped<NoteRepository> {
                NoteRepositoryImpl(androidApplication(), getSource(), get(), get())
            }
            scoped { NoteSyncLogic(getSource(), get(), get(), get()) }

            viewModel { MainFragmentViewModel(androidApplication(), this) }
            viewModel { NotesViewModel(androidApplication(), this) }
            viewModel { NoteEditorViewModel(get(), androidApplication(), this) }
        }
    }
}