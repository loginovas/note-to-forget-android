package iloginovas.notetoforget

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.commit
import iloginovas.notetoforget.data.LoginResult
import iloginovas.notetoforget.data.Preferences
import iloginovas.notetoforget.data.UserSession
import iloginovas.notetoforget.ui.login.LoginScreenFragment
import iloginovas.notetoforget.ui.MainFragment
import org.koin.android.ext.android.inject

class MainActivity : AppCompatActivity(),
    MainFragment.Callback,
    LoginScreenFragment.Callback {

    private val prefs: Preferences by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            if (prefs.userSession.value == null) {
                setLoginScreenFragment()
            } else {
                setMainScreenFragment()
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return false
    }

    private fun setLoginScreenFragment() {
        supportFragmentManager.commit {
            val fragment = LoginScreenFragment.newInstance()
            replace(R.id.main_activity_fragment_container, fragment)
            setReorderingAllowed(true)
        }
    }

    private fun setMainScreenFragment() {
        supportFragmentManager.commit {
            val fragment = MainFragment.newInstance()
            replace(R.id.main_activity_fragment_container, fragment)
            setReorderingAllowed(true)
            setPrimaryNavigationFragment(fragment)
        }
    }

    override fun loggedIn(loginResult: LoginResult.Successful) {
        prefs.userSession.value = UserSession(loginResult.user.id, loginResult.sessionId)
        prefs.username.value = loginResult.user.name
        setMainScreenFragment()
    }

    override fun loggedOut() {
        prefs.userSession.value = null
        prefs.username.value = null
        setLoginScreenFragment()
    }
}