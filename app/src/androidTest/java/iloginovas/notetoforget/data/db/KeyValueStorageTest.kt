@file:Suppress("MemberVisibilityCanBePrivate")

package iloginovas.notetoforget.data.db

import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import iloginovas.notetoforget.App
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsNull.nullValue
import org.junit.After
import org.junit.Assert.assertArrayEquals
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.util.*
import kotlin.random.Random

@RunWith(AndroidJUnit4::class)
class KeyValueStorageTest {

    lateinit var db: AppDatabase
    lateinit var storage: KeyValueStorage

    @Before
    fun init() {
        val app = ApplicationProvider.getApplicationContext<App>()
        db = AppDatabase.createInMemory(app)
        storage = KeyValueStorageImpl(db)
    }

    @After
    fun destroy() {
        db.close()
    }

    @Suppress("BooleanLiteralArgument")
    @Test
    fun test() {
        // Long
        testCRUD(Random.nextLong(), Random.nextLong())
        testCRUD(0L, 1L)
        testCRUD(Long.MIN_VALUE)
        testCRUD(Long.MAX_VALUE)

        // Int
        testCRUD(Random.nextInt(), Random.nextInt())
        testCRUD(0, 1)
        testCRUD(Int.MIN_VALUE)
        testCRUD(Int.MAX_VALUE)

        // Float
        testCRUD(Random.nextFloat(), Random.nextFloat())
        testCRUD(0.0F, 1.1F)
        testCRUD(Float.MIN_VALUE)
        testCRUD(Float.MAX_VALUE)

        // Double
        testCRUD(Random.nextDouble(), Random.nextDouble())
        testCRUD(0.0, 1.1)
        testCRUD(Double.MIN_VALUE)
        testCRUD(Double.MAX_VALUE)

        // ByteArray
        testCRUD(Random.nextBytes(64), Random.nextBytes(48))
        testCRUD(byteArrayOf(1, 2, 3, 4, 46))
        testCRUD(byteArrayOf())

        // String
        testCRUD("", "")
        testCRUD("str", "")
        testCRUD("", "str")
        testCRUD("str", "str")
        testCRUD(randomString(), randomString())

        // Boolean
        testCRUD(false, false)
        testCRUD(false, true)
        testCRUD(true, false)
        testCRUD(true, true)
    }

    private fun randomString(): String = UUID.randomUUID().toString()

    private inline fun <reified T : Any> testCRUD(
        value1: T,
        value2: T? = null,
        key: String = randomString()
    ) {
        storage.put(key, value1)
        assertThatCurrentValueEqualTo(key, value1)

        storage.put(key, value2)
        assertThatCurrentValueEqualTo(key, value2)

        storage.remove(key)
        assertThat(storage.get<T>(key), nullValue())
    }

    private inline fun <reified T : Any> assertThatCurrentValueEqualTo(key: String, expected: T?) {
        val actual: T? = storage.get(key)
        when (expected) {
            is Float -> assertEquals(expected, actual as Float, 0.0001F)
            is Double -> assertEquals(expected, actual as Double, 0.0001)
            is ByteArray -> assertArrayEquals(expected, actual as ByteArray)
            else -> assertEquals(expected, actual)
        }
    }
}