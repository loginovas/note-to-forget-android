package iloginovas.notetoforget.common.validation

import iloginovas.notetoforget.common.validation.PasswordErrorCause.*
import iloginovas.notetoforget.common.validation.PasswordSpec.maxLength
import iloginovas.notetoforget.common.validation.PasswordSpec.minLength
import org.junit.Test

class PasswordValidatorTest : AbstractValidatorTest<String, PasswordErrorCause>() {

    override val validator = PasswordValidator

    @Test
    fun test() {
        PASSWORD_IS_EMPTY.let {
            input("") shouldProduce it
            input("   ") shouldProduce it
            input(" ".repeat(PasswordSpec.maxLength + 1)) shouldProduce it
        }

        input(" qwerty 123456 ").shouldNotProduceErrors()
        input(PasswordSpec.specialCharacters).shouldNotProduceErrors()

        input("абвгдфбвгд") shouldProduce PASSWORD_CONTAINS_INVALID_CHARACTERS

        input(" абв ") {
            shouldProduce(PASSWORD_CONTAINS_INVALID_CHARACTERS)
            shouldProduce(PASSWORD_IS_TOO_SHORT)
        }

        PASSWORD_IS_TOO_SHORT.let {
            input("123") shouldProduce it
            input("n".repeat(minLength - 1)) shouldProduce it
            input("   " + "n".repeat(minLength - 1) + "   ") shouldProduce it
        }

        PASSWORD_IS_TOO_LONG.let {
            input("n".repeat(maxLength + 1)) shouldProduce it
            input("   " + "n".repeat(maxLength) + "   ") shouldNotProduce it
        }
    }
}