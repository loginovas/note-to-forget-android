package iloginovas.notetoforget.common.validation

/**
 * [T] is a type of what we want to validate]
 */
abstract class AbstractValidatorTest<T, E : ErrorCause> {

    abstract val validator: Validator<T, E>

    protected fun input(vararg input: T): List<T> = listOf(*input)

    protected inline fun input(vararg input: T, testFun: List<T>.() -> Unit) {
        listOf(*input).testFun()
    }

    protected infix fun Iterable<T>.shouldProduce(cause: E) {
        forEach { value ->
            val errorCauses = validator.validateVerbose(value)
            assert(errorCauses.contains(cause)) {
                "Validation should produces error $cause for input '$value'"
            }
        }
    }

    protected infix fun Iterable<T>.shouldNotProduce(cause: E) {
        forEach { value ->
            val errorCauses = validator.validateVerbose(value)
            assert(!errorCauses.contains(cause)) {
                "Validation should not produces error $cause for input '$value'"
            }
        }
    }

    protected fun Iterable<T>.shouldNotProduceErrors() {
        forEach { value ->
            val errorCauses = validator.validateVerbose(value)
            assert(errorCauses.isEmpty()) {
                "Validation should not produce errors for input '$value'"
            }
        }
    }

    protected fun Iterable<T>.shouldProduceErrors() {
        forEach { value ->
            val errorCauses = validator.validateVerbose(value)
            assert(errorCauses.isNotEmpty()) {
                "Validation should produce errors for input '$value'"
            }
        }
    }
}