package iloginovas.notetoforget.common.validation

import iloginovas.notetoforget.common.validation.UsernameErrorCause.*
import org.junit.Test

class UsernameValidatorTest : AbstractValidatorTest<String, UsernameErrorCause>() {

    override val validator = UsernameValidator

    private val exceededLength = UsernameSpec.maxLength + 1

    private val validInputs = listOf(
        "Andrey",
        "Andrey_Liginov",
        "_Andrey_Loginov_2020",
        "Andrey2020",
        "A_B_C_2020",
        "_0_0____Andrey"
    )
    private val invalidInputs = listOf(
        "",
        "_",
        "1",
        "_2020",
        "2Andrey",
        "2_Andrey",
        "Andrey\nLoginov",
        "Andrey\tLoginov",
        "A".repeat(exceededLength)
    )

    @Test
    fun testCommonCases() {
        validInputs.shouldNotProduceErrors()
        invalidInputs.shouldProduceErrors()
    }

    @Test
    fun testSpecialCases() {
        input("") shouldProduce USERNAME_IS_EMPTY
        input("_", "1", "@", "0ABC", "A", "2020") {
            shouldNotProduce(USERNAME_IS_EMPTY)
        }

        input("@", "%", "$", "ABC.2020", "ABD*2020", "ABC-2020") {
            shouldProduce(USERNAME_CONTAINS_INVALID_CHARACTERS)
        }
        input("Abc_2020", "Abc_2020", "0Abc", "", "_", "1") {
            shouldNotProduce(USERNAME_CONTAINS_INVALID_CHARACTERS)
        }

        input("A".repeat(exceededLength)) {
            shouldProduce(MAX_LENGTH_EXCEEDED)
        }
        invalidInputs.filter { it.length <= 30 } shouldNotProduce MAX_LENGTH_EXCEEDED

        input("0_Alex", "1A", "121") shouldProduce FIRST_CHARACTER_IS_NUMBER
        invalidInputs.filter {
            when {
                it.isEmpty() -> true
                else -> !it.first().isDigit()
            }
        } shouldNotProduce FIRST_CHARACTER_IS_NUMBER

        input("_1_2_3", "_", "_0___0_") {
            shouldProduce(USERNAME_SHOULD_CONTAIN_AT_LEAST_ONE_LETTER)
        }
        input("_A", "Ann", "Leo_12", "_ANN_", "0_Alex") {
            shouldNotProduce(USERNAME_SHOULD_CONTAIN_AT_LEAST_ONE_LETTER)
        }
    }
}