package iloginovas.notetoforget.common

import org.junit.Test
import kotlin.random.Random

class NoteIdTest {

    private val random = Random(System.nanoTime())

    private val validTimeValues: List<Long>
    private val validRandomNumbers: List<Long>

    init {
        validTimeValues = arrayListOf(
            NoteId.MIN_TIME_VALUE, NoteId.MIN_TIME_VALUE + 1, NoteId.MIN_TIME_VALUE + 100,
            NoteId.MAX_TIME_VALUE, NoteId.MAX_TIME_VALUE - 1, NoteId.MAX_TIME_VALUE - 100,
        )
        repeat(100) {
            val randomTime = NoteId.TIME_RANGE.random(random)
            validTimeValues.add(randomTime)
        }

        validRandomNumbers = arrayListOf(
            NoteId.MIN_RANDOM_VALUE, NoteId.MIN_RANDOM_VALUE + 1, NoteId.MIN_RANDOM_VALUE + 100,
            NoteId.MAX_RANDOM_VALUE, NoteId.MAX_RANDOM_VALUE - 1, NoteId.MAX_RANDOM_VALUE - 100
        )
        repeat(100) {
            val randomNum = NoteId.RANDOM_NUMBER_RANGE.random(random)
            validRandomNumbers.add(randomNum)
        }
    }

    @Test
    fun test() {
        validTimeValues.forEach { time ->
            validRandomNumbers.forEach { randomNum ->
                createIdAndCheck(time, randomNum)
            }
        }
    }

    private fun createIdAndCheck(time: Long, randomNum: Long) {
        try {
            val id: Long = NoteId.create(time, randomNum)
            assert(time == NoteId.getSecondsSinceEpoch(id))
            assert(randomNum == NoteId.getRandomPart(id))
        } catch (e: Throwable) {
            throw AssertionError("NoteId create error [time=$time, random=$randomNum]", e)
        }
    }

    @Test
    fun whenTimeIsOutOfRangeThenException() {
        assertThrows {
            NoteId.create(-1, 0)
        }
        assertThrows {
            NoteId.create(NoteId.MAX_TIME_VALUE + 1, 0)
        }
        assertThrows {
            NoteId.create(0, -1)
        }
        assertThrows {
            NoteId.create(0, NoteId.MAX_RANDOM_VALUE + 1)
        }
    }

    @Test
    fun testThatIdsAreSortedByTime() {
        val sortedTimes: List<Long> = validTimeValues.sorted()

        val ids = ArrayList<Long>(sortedTimes.size)
        sortedTimes.forEach {
            val id = NoteId.create(it, NoteId.nextRandomNumber())
            ids.add(id)
        }

        val sortedIds = ids.sorted()
        for (i in sortedTimes.indices) {
            val time = NoteId.getSecondsSinceEpoch(sortedIds[i])
            assert(time == sortedTimes[i])
        }
    }

    private inline fun assertThrows(code: () -> Unit) {
        try {
            code()
            throw AssertionError("Expected exception but nothing was thrown")
        } catch (e: Throwable) {
        }
    }
}