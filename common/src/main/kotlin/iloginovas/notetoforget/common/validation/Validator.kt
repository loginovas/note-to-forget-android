package iloginovas.notetoforget.common.validation

interface ErrorCause

interface Validator<T, E : ErrorCause> {

    /**
     * Returns [Sequence] through which you receive errors (or nothing if there are not errors).
     **/
    fun errorSequence(value: T): Sequence<E>

    fun validate(value: T): E? =
        errorSequence(value).firstOrNull()

    fun validateVerbose(value: T): List<E> =
        errorSequence(value).toList()
}