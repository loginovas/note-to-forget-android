package iloginovas.notetoforget.common.validation

import iloginovas.notetoforget.common.validation.UsernameErrorCause.*
import iloginovas.notetoforget.common.validation.UsernameSpec.maxLength

object UsernameSpec {
    const val maxLength = 30
}

object UsernameValidator : Validator<String, UsernameErrorCause> {

    private val regex = "^[a-zA-Z_0-9]*$".toRegex()
    private val englishLetterRegex = "[a-zA-Z]".toRegex()

    override fun errorSequence(value: String): Sequence<UsernameErrorCause> =
        sequence {
            val username = value.trim()

            if (username.isBlank())
                yield(USERNAME_IS_EMPTY)

            if (!username.matches(regex))
                yield(USERNAME_CONTAINS_INVALID_CHARACTERS)

            if (username.length > maxLength)
                yield(MAX_LENGTH_EXCEEDED)

            if (username.isNotEmpty() && username.first().isDigit())
                yield(FIRST_CHARACTER_IS_NUMBER)

            if (!username.contains(englishLetterRegex)) {
                yield(USERNAME_SHOULD_CONTAIN_AT_LEAST_ONE_LETTER)
            }
        }
}

/**
 * @param errorMsg is default error message
 **/
enum class UsernameErrorCause(val errorMsg: String) : ErrorCause {

    USERNAME_IS_EMPTY("Username is empty"),

    USERNAME_CONTAINS_INVALID_CHARACTERS(
        "Username contains invalid characters. " +
                "English letters, underscore and decimal numbers are only allowed"
    ),

    MAX_LENGTH_EXCEEDED("Max length of username is $maxLength"),

    FIRST_CHARACTER_IS_NUMBER("Username should not start with number"),

    USERNAME_SHOULD_CONTAIN_AT_LEAST_ONE_LETTER("Username should contain at least one letter");
}