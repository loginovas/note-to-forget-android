package iloginovas.notetoforget.common.validation

import iloginovas.notetoforget.common.validation.PasswordSpec.maxLength
import iloginovas.notetoforget.common.validation.PasswordSpec.minLength
import iloginovas.notetoforget.common.validation.PasswordSpec.specialCharacters
import java.util.regex.Pattern

object PasswordSpec {
    const val maxLength = 100
    const val minLength = 6
    const val specialCharacters: String = """ !"#${'$'}%&'()*+,-./:;<=>?@[\]^_`{|}~"""
}

object PasswordValidator : Validator<String, PasswordErrorCause> {

    private val escapedSpecCharacters = Pattern.quote(specialCharacters)
    private val pattern: Regex = """^[a-zA-Z_0-9$escapedSpecCharacters]*$""".toRegex()

    override fun errorSequence(value: String): Sequence<PasswordErrorCause> =
        sequence {
            val password = value.trim()

            if (password.isBlank()) {
                yield(PasswordErrorCause.PASSWORD_IS_EMPTY)
                return@sequence
            }

            if (password.length < minLength)
                yield(PasswordErrorCause.PASSWORD_IS_TOO_SHORT)
            else if (password.length > maxLength)
                yield(PasswordErrorCause.PASSWORD_IS_TOO_LONG)

            if (!password.matches(pattern)) {
                yield(PasswordErrorCause.PASSWORD_CONTAINS_INVALID_CHARACTERS)
            }
        }
}

/**
 * @param errorMsg is default error message
 **/
enum class PasswordErrorCause(val errorMsg: String) : ErrorCause {

    PASSWORD_IS_EMPTY("Password should be specified"),

    PASSWORD_CONTAINS_INVALID_CHARACTERS(
        "Password contains invalid characters. " +
                "English letters, underscore, space and decimal numbers are allowed." +
                "Also these special characters are allowed: $specialCharacters"
    ),

    PASSWORD_IS_TOO_LONG("Max length of password is $maxLength"),

    PASSWORD_IS_TOO_SHORT("Min length of password is $minLength"),
}