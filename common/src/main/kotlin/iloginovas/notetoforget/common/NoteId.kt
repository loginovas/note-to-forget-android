package iloginovas.notetoforget.common

import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.random.Random

/**
 * The time ordered primary key consists of a timestamp and a random number.
 * ---
 * - The timestamp is the count of **seconds** since epoch (January 1, 1970, 00:00:00 GMT).
 * - The ID is stored in [Long] and the time order is provided out of the box by using [Long.compareTo].
 * - ID's Long value cannot be negative.
 * - Max value of time corresponds to 7 Feb 2106 06:28:15 GMT
 **/
@Suppress("MemberVisibilityCanBePrivate", "unused")
class NoteId {

    companion object {

        const val MIN_TIME_VALUE: Long = 0L
        const val MAX_TIME_VALUE: Long = (1L shl 32) - 1
        val TIME_RANGE = LongRange(MIN_TIME_VALUE, MAX_TIME_VALUE)

        const val MIN_RANDOM_VALUE: Long = 0L
        const val MAX_RANDOM_VALUE: Long = (1L shl 31) - 1
        val RANDOM_NUMBER_RANGE = LongRange(MIN_RANDOM_VALUE, MAX_RANDOM_VALUE)

        /** 31 bits for random number */
        val RANDOM_NUMBER_BITS = (0 until 31)

        /** Timestamp part is 32 bits. Leftmost 64th bit is used for sign (don't use it) */
        val TIME_BITS = (31 until 64)

        private val random = Random(System.nanoTime())

        fun getTimeSinceEpoch(id: Long, timeUnit: TimeUnit) =
            timeUnit.convert(getSecondsSinceEpoch(id), TimeUnit.SECONDS)

        fun getMillisSinceEpoch(id: Long) =
            getTimeSinceEpoch(id, TimeUnit.MILLISECONDS)

        fun getSecondsSinceEpoch(id: Long): Long {
            check(id >= 0) { "Invalid ID: should not be negative " }
            return id.ushr(TIME_BITS.first)
        }

        fun generate(date: Date): Long =
            generate(date.time, TimeUnit.MILLISECONDS)

        fun generate(time: Long, timeUnit: TimeUnit): Long =
            generate(timeUnit.toSeconds(time))


        fun generate(secondsSinceEpoch: Long): Long {
            val randomPart: Long = nextRandomNumber()
            return create(secondsSinceEpoch, randomPart)
        }

        fun create(secondsSinceEpoch: Long, randomNumber: Long): Long {
            require(secondsSinceEpoch in TIME_RANGE) {
                "secondsSinceEpoch should be in [$TIME_RANGE]"
            }
            require(randomNumber in RANDOM_NUMBER_RANGE) {
                "randomNumber should be in [$RANDOM_NUMBER_RANGE]"
            }

            val timePart: Long = secondsSinceEpoch.shl(TIME_BITS.first)
            return randomNumber.or(timePart)
        }

        fun nextRandomNumber(): Long =
            RANDOM_NUMBER_RANGE.random(random)

        fun getRandomPart(id: Long): Long {
            val mask: Long = 0b1111111111111111111111111111111 // 31 rightmost bits
            return mask.and(id)
        }
    }
}