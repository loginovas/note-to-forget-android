package iloginovas.notetoforget.common.login

import iloginovas.notetoforget.common.validation.PasswordErrorCause
import iloginovas.notetoforget.common.validation.UsernameErrorCause

sealed interface LoginErrorCause

class InvalidFormat(
    val usernameError: UsernameErrorCause? = null,
    val passwordError: PasswordErrorCause? = null
) : LoginErrorCause

object WrongCredentials : LoginErrorCause
object UsernameAlreadyExists : LoginErrorCause