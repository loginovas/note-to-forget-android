package iloginovas.notetoforget.common

import iloginovas.notetoforget.common.login.InvalidFormat
import iloginovas.notetoforget.common.login.LoginErrorCause
import iloginovas.notetoforget.common.login.UsernameAlreadyExists
import iloginovas.notetoforget.common.login.WrongCredentials

@Suppress("unused")
object Api {
    data class UserCredentials(val username: String, val password: String)

    data class User(val id: Int, val name: String)

    class LoginResponse(
        val onSuccess: OnSuccess? = null,
        val onFailure: OnFailure? = null
    ) {
        class OnSuccess(val sessionId: String, val user: User)
        class OnFailure(
            val invalidFormat: InvalidFormat? = null,
            val wrongCredentials: WrongCredentials? = null,
            val usernameAlreadyExists: UsernameAlreadyExists? = null
        )

        companion object {
            fun failure(error: LoginErrorCause): LoginResponse {
                val onFailure = when (error) {
                    is InvalidFormat -> OnFailure(invalidFormat = error)
                    is WrongCredentials -> OnFailure(wrongCredentials = WrongCredentials)
                    is UsernameAlreadyExists -> OnFailure(usernameAlreadyExists = UsernameAlreadyExists)
                }
                return LoginResponse(onFailure = onFailure)
            }

            fun success(sessionId: String, user: User) =
                LoginResponse(onSuccess = OnSuccess(sessionId, user))
        }
    }

    data class Note(
        /** @see NoteId */
        val id: Long,

        val title: String,
        val text: String,

        /** Seconds since epoch */
        val lastUpdate: Long
    )

    data class NoteChanges(
        val version: Long,
        val notesToBeRemoved: List<Long>,

        /** Created or updated notes */
        val notesToBePut: List<Note>
    )
}

val Api.LoginResponse.OnFailure.error: LoginErrorCause
    get() = when {
        invalidFormat != null -> invalidFormat
        wrongCredentials != null -> wrongCredentials
        usernameAlreadyExists != null -> usernameAlreadyExists
        else -> throw IllegalStateException()
    }
